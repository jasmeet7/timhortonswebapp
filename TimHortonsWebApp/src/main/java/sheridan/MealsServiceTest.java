package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class MealsServiceTest {

	@Test
	public void testDrinksRegular() {
		MealsService ms = new MealsService();
		List<String> meals = ms.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("Number of drinks were more than 0", meals.size() > 0);
	}
	
	@Test
	public void testDrinksException() {
		MealsService ms = new MealsService();
		List<String> meals = ms.getAvailableMealTypes(null);
		assertTrue("No Brands available", meals.get(0).equals("No Brand Available"));
	}

	@Test
	public void testDrinksBoundaryIn() {
		MealsService ms = new MealsService();
		List<String> meals = ms.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("Number of drinks were more than 3", meals.size() > 3);
	}
	
	@Test
	public void testDrinksBoundaryOut() {
		MealsService ms = new MealsService();
		List<String> meals = ms.getAvailableMealTypes(null);
		assertTrue("Only 1 result was returned", meals.size() == 1);
	}
}
